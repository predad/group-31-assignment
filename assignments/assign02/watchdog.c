#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/watchdog.h"


/**
 * @brief wrapper function to call watchdog_update() in Assembly
 * 
 */
void asm_watchdog_update() {
    watchdog_update();
}

/**
 * @brief Enable the watchdog with a time in milliseconds
 * 
 * @param time The time the watchdog need to update constantly else reboot
 */
void init_watchdog(uintptr_t time){
    // pause on debug is enabled
    watchdog_update(time, 1)
}

int main() {
    stdio_init_all();
    if (watchdog_caused_reboot()) {
        printf("Game Restarted\n");
        return 0;
    } else {
        printf("Clean boot\n");
    }
}
