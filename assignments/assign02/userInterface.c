#include <stdio.h>
#include "pico/stdlib.h"

// int main() {
//     stdio_init_all();
//     currentLevel(3, 3, "BEE", "-... . .", "----- -----");
//     return(0);
// }


/**
 * @brief Methods for displaying the UI of the morse code game
 * @author Daniel Padmore
 * 
 */


// prints the main menu of the game
void mainMenu() {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|   A: .-    B: -...  C: -.-.  D: -..   E: .     |\n");
    printf("|   F: ..-.  G: --.   H: ....  I: ..    J: .---  |\n");
    printf("|   K: -.-   L: .-..  M: --    N: -.    O: ---   |\n");
    printf("|   P: .--.  Q: --.-  R: .-.   S: ...   T: -     |\n");
    printf("|   U: ..-   V: ...-  W: .--   X: -..-  Y: -.--  |\n");
    printf("|   Z: --..  0: ----- 1: .---- 2: ..--- 3: ...-- |\n");
    printf("|   4: ....- 5: ..... 6: -.... 7: --... 8: ---.. |\n");
    printf("|   9: ----.                                     |\n");
    printf("+------------------------------------------------+\n");
    printf("|      USE GP21 TO ENTER A SEQUENCE TO START     |\n");
    printf("|        \".\"  - LEVEL 01 - CHARS (EASY)          |\n");
    printf("|        \"-\"  - LEVEL 02 - CHARS (HARD)          |\n");
    printf("|        \".-\" - LEVEL 03 - WORDS (EASY)          |\n");
    printf("|        \"-.\" - LEVEL 04 - WORDS (HARD)          |\n");
    printf("+------------------------------------------------+\n");
    printf("| Instruction:                                    |\n");
    printf("| Input the Required Morse Code and Wait for      |\n");
    printf("| 3 seconds to progress to the input the next     |\n");
    printf("| Morse Code Sequence                             |\n");
    printf("| Short press of GPIO 21 is \".\"                  |\n");
    printf("| Long press ( >0.5 seconds) of GPIO 21 is \"-\"   |\n");
    printf("+------------------------------------------------+\n");
}

// advancing level screen
void nextLevel(int lives, int currentLevel) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|             ADVANCING TO LEVEL %d...            |\n", currentLevel);
    printf("|                    LIVES: %d                    |\n", lives);
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
}


void printWrongAnswerInput(char wanted, char decoded, char* morseCodeWanted, char* morseCodeEntered) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|            Incorrect Morse Code Entered!       |\n");
    printf("|            Wanted: \"%c\"                      |\n", wanted);
    printf("|            You Entered: \"%c\"                 |\n", decoded);
    printf("|                                                |\n");
    printf("|            Wanted: \"%s\"                      |\n", morseCodeWanted);
    printf("|            You Entered: \"%s\"                 |\n", morseCodeEntered);
    printf("|                       Live - 1                 |\n");
    printf("|                      Next Round                |\n");
    printf("+------------------------------------------------+\n");

}

void printCorrectAnswerInput(char wanted, char decoded, char* morseCodeWanted, char* morseCodeEntered, int lives) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|            Correct Morse Code Entered!         |\n");
    printf("|            Wanted: \"%c\"                         |\n", wanted);
    printf("|            You Entered: \"%c\"                    |\n", decoded);
    printf("|                                                |\n");
    printf("|            Wanted: \"%s\"                      |\n", morseCodeWanted);
    printf("|            You Entered: \"%s\"                 |\n", morseCodeEntered);    
    if(lives < 3)
    {
        printf("|                       Live + 1                 |\n"); 
    }
    printf("|                      Next Round                |\n");
     printf("+------------------------------------------------+\n");
}
// prints the current level of the game
// randomString is the string presented to the user which they have to implement, userInputtedCharacter is the string that the user inputs
void currentLevel(int currentLevel, int lives, char* randomAlphanumericString, char** hint, char* userInputtedCharacter) {
    char* randomOffset = "\t\t\t\t";
    if(strlen(randomAlphanumericString) == 5) {
        randomOffset = "\t\t\t";
    }
    printf("+------------------------------------------------+\n");
    printf("|               CURRENT LEVEL: 0%d                |\n",currentLevel);
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("| INPUT NEEDED:  \"%s\" %s |\n", randomAlphanumericString, randomOffset);
    printf("|                                                |\n");
    if(hint == NULL) {
        printf("| HINT PROVIDED: NONE                            |\n");
    }
    else {
        printf("  HINT PROVIDED: ", hint);
        int len = strlen(randomAlphanumericString);
        for( int i = 0; i < len; i++){
            printf("\"%s\"  ", hint[i]);
        }
    }
    printf("\n|                                                |\n");
    if(currentLevel%2 != 0){
        if(userInputtedCharacter[0] != '?'){
            if(strlen(userInputtedCharacter) == 0) {
                printf("|                                                |\n");
            }
            else {
                printf("| YOU ENTERED: \"%s\"",userInputtedCharacter);
                int i = strlen(userInputtedCharacter) + 18;
                while(i < 48) {
                    printf(" ");
                    i++;
                }
                printf(" |\n");
            }
        }
    }
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                   LIVES: %d                     |\n", lives);
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
}

// prints the win screen
// winStreak is the highest streak of correctAnswers during the game
void winScreen(int winStreak, int correctAnswers, int questions) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                CONGRATULATIONS!                |\n");
    printf("|                    YOU WIN                     |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
    printf("|                   STATISTICS                   |\n");
    float accuracy = (float) correctAnswers / (float) questions;
    accuracy *= 100;
    if(winStreak > 9) {
        printf("| HIGHEST WINSTREAK: %d                          |\n", winStreak);
    }
    else {
        printf("| HIGHEST WINSTREAK: %d                           |\n", winStreak);
    }
    if(correctAnswers > 9) {
        printf("| CORRECT ANSWERS: %d                            |\n", correctAnswers);
    }
    else {
        printf("| CORRECT ANSWERS: %d                             |\n", correctAnswers);
    }
    if(accuracy == 100) {
        printf("| ACCURACY: %d%%                                 |\n", (int) accuracy);
    }
    else {
        printf("| ACCURACY: %d%%                                  |\n", (int) accuracy);
    }
    printf("| Total Round Played: %d                              |\n", questions);
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
}

// prints the lose screen when out of lives
// winStreak is the highest streak of correctAnswers during the game
void loseScreenLives(int winStreak, int correctAnswers, int questions) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                  OUT OF LIVES!                 |\n");
    printf("|                    YOU LOSE                    |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
    printf("|                   STATISTICS                   |\n");
    float accuracy = (float) correctAnswers / (float) questions;
    accuracy *= 100;
    if(winStreak > 9) {
        printf("| HIGHEST WINSTREAK: %d                          |\n", winStreak);
    }
    else {
        printf("| HIGHEST WINSTREAK: %d                           |\n", winStreak);
    }
    if(correctAnswers > 9) {
        printf("| CORRECT ANSWERS: %d                            |\n", correctAnswers);
    }
    else {
        printf("| CORRECT ANSWERS: %d                             |\n", correctAnswers);
    }
    if(accuracy == 100) {
        printf("| ACCURACY: %d%%                                 |\n", (int) accuracy);
    }
    else {
        printf("| ACCURACY: %d%%                                  |\n", (int) accuracy);
    }
    printf("| Total Round Played: %d                              |\n", questions);
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
}

// prints the lose screen when timer runs out
// winStreak is the highest streak of correctAnswers during the game
void loseScreenTime(int winStreak, int correctAnswers, int questions) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                  OUT OF TIME!                  |\n");
    printf("|                    YOU LOSE                    |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
    printf("|                   STATISTICS                   |\n");
    float accuracy = (float) correctAnswers / (float) questions;
    accuracy *= 100;
    if(winStreak > 9) {
        printf("| HIGHEST WINSTREAK: %d                          |\n", winStreak);
    }
    else {
        printf("| HIGHEST WINSTREAK: %d                           |\n", winStreak);
    }
    if(correctAnswers > 9) {
        printf("| CORRECT ANSWERS: %d                            |\n", correctAnswers);
    }
    else {
        printf("| CORRECT ANSWERS: %d                             |\n", correctAnswers);
    }
    if(accuracy == 100) {
        printf("| ACCURACY: %d%%                                 |\n", (int) accuracy);
    }
    else {
        printf("| ACCURACY: %d%%                                  |\n", (int) accuracy);
    }
    printf("| Total Round Played: %d                              |\n", questions);
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
}


void printStatisticBeforeNextLevel(int winStreak, int correctAnswers, int questions) {
    printf("+------------------------------------------------+\n");
    printf("|            GROUP 31 MORSE CODE GAME            |\n");
    printf("+------------------------------------------------+\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                  YOU DID IT!                   |\n");
    printf("|            Progress to Next Level              |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
    printf("|                   STATISTICS                   |\n");
    float accuracy = (float) correctAnswers / (float) questions;
    accuracy *= 100;
    if(winStreak > 9) {
        printf("| HIGHEST WINSTREAK: %d                          |\n", winStreak);
    }
    else {
        printf("| HIGHEST WINSTREAK: %d                           |\n", winStreak);
    }
    if(correctAnswers > 9) {
        printf("| CORRECT ANSWERS: %d                            |\n", correctAnswers);
    }
    else {
        printf("| CORRECT ANSWERS: %d                             |\n", correctAnswers);
    }
    if(accuracy == 100) {
        printf("| ACCURACY: %d%%                                 |\n", (int) accuracy);
    }
    else {
        printf("| ACCURACY: %d%%                                  |\n", (int) accuracy);
    }
    printf("| Total Round Played In This LEVEL: %d                |\n", questions);
    printf("|                                                |\n");
    printf("+------------------------------------------------+\n");
}

