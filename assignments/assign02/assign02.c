#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "pico/time.h"
#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/watchdog.h"
#include "rgb.c"
#include "userInterface.c"

#define WATCHDOG_TIME 9000
// global variables:
const char alpha_numeric[] ="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
char alpha_numeric_c[] ="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
char * alpha_numeric_s[]={"CAT","WHO","DOG","PEN","PIE","BE","US","ME","MAY","CUP","LAB","BAG",
"TOE","ARM","LEG","BEE","HAT","PET","CAN","ADD","MAN","EAR","BIN","CAR","SUN","BOX","MAP","FLY","FAN","5RAT","1OWL","9BEE","2COW","3PIG"};
const int LEVEL1 = 1;
const int LEVEL2 = 2;
const int LEVEL3 = 3;
const int LEVEL4 = 4;
int lives = 3;
int totalQuestions = 0; // number of round played
int totalWin = 0; // the number of round that are won
int maxWinningStreaks = 0; 

int correctCharacterEntered = 0;
int totalCharacterEntered = 0;

char* morseCode[] = {
        ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."
    };

char* number[] = { "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."};


/**
 * @brief Decode the morse code sequence to an alphanumeric character
 * 
 * @param userInputMorseCode a string of morse code sequence
 * @return a char which is the decoded alphanumeric character
 */
char decoder(char* userInputMorseCode) {
    int index = 0;
    int isAlphabet = -1; // -1 means invalid input
    for(; index < 26; index++){
        if(strcmp(userInputMorseCode, morseCode[index]) == 0){
            isAlphabet = 1;
            break;
        }
    }
    if(index == 26){
        index = 0;
        for(; index < 10; index++){
            if(strcmp(userInputMorseCode, number[index]) == 0){
                isAlphabet = 0;
                break;
            }
        }
    }
    char ans = '?';
    if(isAlphabet == 1) {
        ans = index + 'A';
    }
    if(isAlphabet == 0){
        ans = index + '0';
    }
    return ans;
}

/**
 * @brief convert an alphanumeric string to an array of morse code string and return it
 * 
 * @param alphanumericString 
 * @return char** a array of morse code strings
 */
char** converter(char * alphanumericString) {
    int len = strlen(alphanumericString);
    char** answer = malloc(sizeof(char *) * len);

    // initialize space for each string in the string array
    for( int i = 0; i < len; i++){
        answer[i] = malloc(sizeof(char) * 7); // morse code have a max length of 7 characters
    }

    for( int i = 0; i < len; i++ ){
        char currentChar = alphanumericString[i];
        // if the currentChar is English Alphabet
        if(currentChar - 'A' >= 0 ){
            char * morseString = morseCode[currentChar - 'A'];
            strcpy(answer[i], morseString); // copy to the answer array
            // answer
        }
        // if currentChar is a number
        else if( currentChar - 'A' < 0){
            char * morseString = number[currentChar - '0'];
            strcpy(answer[i], morseString); // copy to the answer array
        }
    }
    return answer;
}

// /**
//  * @brief compare the 
//  * 
//  * @param correctAnswer 
//  * @param userInputs 
//  * @param arrayLength 
//  * @return int 
//  */
// int compareAnswer(char** correctAnswer, char** userInputs, int arrayLength){
//     for( int i = 0; i < arrayLength; i++ ){
//         if (strcmp(correctAnswer[i], userInputs[i]) != 0) {
//             return 0; // return false
//         }
//     }
//     return 1; // default is return true
// }


// int compareString(char* correctAnswer, char* userInputs){
//     if(strcmp(correctAnswer, userInputs) == 0) {
//         return 1;
//     }
//     return 0;
// }


/**
 * @brief wrapper function to call watchdog_update() in Assembly
 * 
 */
void asm_watchdog_update() {
    watchdog_update();
}
/**
 * @brief Enable the watchdog with a time in milliseconds
 * 
 * @param time The time the watchdog need to update constantly else reboot
 */
void init_watchdog(uintptr_t time){
    // pause on debug is enabled
    watchdog_enable(time, 1);
}

// Must declare the main assembly entry point before use. 
char* main_asm(int string_length);


/**
 * @brief Runs the morse input code and parses the output string into an array of strings
 * @author Dan Preda
 * @param number_of_strings number of morse strings that correspond to the length of the alphanumeric string
 * @return A string array, which each string being a morse string to be decoded into a single alphanumeric character 
 */
char** morse_string_array(int number_of_strings){   
    // printf("To input your answer, use a short button press for \'.\',and a long one for \'-\'.\nTo move on to the next morse string, wait for 3 seconds before continueing\nWhen you are finished your last morse string, wait 3 seconds and press the button again to exit\n");
    char* string = main_asm(number_of_strings-1);
    // printf("Returned from asembly\n");
    if(string[0] == 0) {printf("Invalid Morse input.");return NULL;}//if the first char is null, input is invalid
    int string_index = 0;
    char** result = malloc(number_of_strings * sizeof(char*));
    for(int i = 0; i<number_of_strings;i++){
        result[i] = (char *)malloc(sizeof(char)*6);
    }
    int num_of_null_chars = 0;

    for(int i = 0; i<20;i++){if(string[i]==0)num_of_null_chars++;}
    if(num_of_null_chars != number_of_strings){printf("Invalid Morse input.");return NULL;}// if there isnt an equal number of morse strings in the output return null

    for(int i = 0; i<number_of_strings;i++){
        char current_string[11];
        int current_string_index = 0;
        bool over_5_chars = false;
        while(string[string_index] != 0){
            if(!over_5_chars){
                char current = string[string_index];
                current_string[current_string_index] = current; // copy the contents of the output string into a char[] until we reach the null char or the input is over 5 chars (morse code for alphanumerics are a maximum of 5 chars long)
                current_string_index++;
            }
            string_index++;
            if(current_string_index>=10){over_5_chars = true;}
        }
        
        current_string[current_string_index] = 0;
        strcpy(result[i],current_string); //copy our parsed string into our array
        string_index++;
    }
    return result;
}

void asm_gpio_init(uint pin){
    gpio_init(pin);
}

void asm_gpio_put(uint pin,bool out){
    gpio_set_dir(pin,out);
}
void asm_gpio_set_irq_fe(uint pin){
    gpio_set_irq_enabled(pin,GPIO_IRQ_EDGE_FALL,true);
}
void asm_gpio_set_irq_re(uint pin){
    gpio_set_irq_enabled(pin,GPIO_IRQ_EDGE_RISE,true);
}
void free_morse_string_array(char ** array,int no_of_strings){
    for(int i = 0; i< no_of_strings;i++){
        free(array[i]);
    }
    free(array);
}
//generate random char
char rand_generate_char(){
    int rand_index = rand()%strlen(alpha_numeric_c);     // create a random index based on the length of the alpha numeric string
    return alpha_numeric_c[rand_index];                    
}

//generate random string 
char * rand_generate_string(){
    // int array_length = sizeof(alpha_numeric_s)/sizeof(char*);
    int rand_index = rand()%sizeof(alpha_numeric_s)/sizeof(char*); // create a random index based on the length of the stringList
    char * rand_str = malloc(sizeof(char*));                       // allocate memory
    rand_str = alpha_numeric_s[rand_index];
    return rand_str;

}

// modify the program so returns rand_generate_c if level is 1 or 2 return rand_generate_s if level is 3 or 4
char * random_generate(int level){
    if (level == LEVEL1||level ==LEVEL2) {
        char * sing_char = malloc(2);
        sing_char[0]=rand_generate_char();
        sing_char[1]='\0';
        return sing_char;
    } else if (level == LEVEL3||level ==LEVEL4) {
        return rand_generate_string();
    } else {
        // Handle invalid level value
        return NULL;
    }
}

/**
 * @brief Convert the morse code sequece the user entered and convert to the 
 *  equivalent level
 * 
 * @param morseCode morse code sequence
 * @return the level the user selected
 */
int getLevel(char * morseCode) {
    char * level1 = ".";
    char * level2 = "-";
    char * level3 = ".-";
    char * level4 = "-.";
    if(strcmp(morseCode, level1)==0) {
        return 1;
    }else if(strcmp(morseCode, level2)==0){
        return 2;
    }else if(strcmp(morseCode, level3)==0){
        return 3;
    }
    else if(strcmp(morseCode, level4)==0){
        return 4;
    }
    return -1;
}



/*
 * Main entry point for the code - simply calls the main assembly function.
 */
int main() {    
    // Initialize I/O
    stdio_init_all();
    // Initialize the PIO for RGB LED
    init_pio();
    // check whether a watchdog rebott occured
    if (watchdog_caused_reboot()) {
        printf("Time out. Game restarts...\n");
    }
    init_watchdog(WATCHDOG_TIME);
    while(true) {
        lives = 3;
        // print out the starting page of the game
        mainMenu();
        // set the colour of the LED to blue
        set_colour(GAME_NOT_STARTED);
        printf("Input:  ");
        char** levels = morse_string_array(1);
        printf("\n");
        char* levelInMorseCode = levels[0];
        // printf("Morse code entered: %s\n", levelInMorseCode);
        int level = getLevel(levelInMorseCode);
        // if the level is not a valid level
        while( level == -1) {
            mainMenu();
            printf("Invalid level. Please try again!\n");
            printf("Input:  ");
            levels = morse_string_array(1);
            printf("\n");
            levelInMorseCode = levels[0];
            level = getLevel(levelInMorseCode);
        }
        // initialize the statistical variables
        int gameOn = 1;
        int currentLevelWinningStreak = 0;
        int currentLevelHighestWinStreak = 0;
        int currentLevelTotalNumberOfRoundsPlayed = 0;
        int currentLevelTotalNumberOfRoundsWon = 0;
        while(gameOn == 1)
        {
            watchdog_update();
            // randomly generate the alphanumeric character/word needed
            char * alphanumericAnswer = random_generate(level);
            // get the length of alphanumric string generated
            int numberOfAlphanumericCharacters = strlen(alphanumericAnswer);
            // get a copy of length
            int len = numberOfAlphanumericCharacters;
            // convert the alphanumeric string to an array of morse code sequences
            char ** morseCodeAnswer = converter(alphanumericAnswer);
            int isThisRoundWon = 1; // 0 is false and 1 is true
            set_colour(lives);
            char * userInputtedCharacter = malloc(sizeof(char) * len + 1);
            // initialize userInpyttedCharacter
            for( int i = 0; i < len; i++){
                userInputtedCharacter[i] = '?';
            }
            userInputtedCharacter[len] = '\0';
            totalQuestions++; // increment the number of round played
            currentLevelTotalNumberOfRoundsPlayed++;
            // while loop that continous get the user's input 
            while( numberOfAlphanumericCharacters > 0){
                // give hint if the user is at an odd level
                char ** hint = (level%2!=0)?morseCodeAnswer:NULL;
                // print the current level string and the revelant info
                currentLevel(level, lives, alphanumericAnswer, hint, userInputtedCharacter);

                // get the user's input
                printf("Input:  ");
                char** userInput = morse_string_array(1);
                printf("\n");
                char decodedMorseCode = decoder(userInput[0]);

                // add the decoded character to the string of the character that the user have inputted
                userInputtedCharacter[len - numberOfAlphanumericCharacters] = decodedMorseCode;
                userInputtedCharacter[len - numberOfAlphanumericCharacters + 1] = '\0';
                // check if the inputted morse code sequence is correct
                int isAnswerCorrect = strcmp(userInput[0], morseCodeAnswer[len - numberOfAlphanumericCharacters]);
                watchdog_update();
                // if the inputed morse code sequence is incorrect or cannot be decoded
                if(decodedMorseCode == '?' || isAnswerCorrect != 0) {
                    printWrongAnswerInput(alphanumericAnswer[len - numberOfAlphanumericCharacters], decodedMorseCode, morseCodeAnswer[len - numberOfAlphanumericCharacters], userInput[0]);
                    lives--;
                    isThisRoundWon = 0; // change to false
                    // update statistic
                    if( currentLevelWinningStreak > maxWinningStreaks){
                        maxWinningStreaks = currentLevelWinningStreak;
                    }
                    if(currentLevelWinningStreak > currentLevelHighestWinStreak) {
                        currentLevelHighestWinStreak = currentLevelWinningStreak;
                    }
                    currentLevelWinningStreak = 0;
                    set_colour(lives); // update the colour of the LED
                    if(lives == 0){
                        gameOn = 0; // finish the game
                        loseScreenLives(maxWinningStreaks, totalWin, totalQuestions);
                        watchdog_update();
                        sleep_ms(5000);
                    }
                    // watchdog_update();
                    numberOfAlphanumericCharacters = 0; // end this round
                    break; // exit the while loop
                }else{
                    // print a message telling the user has a correct character inputted
                    printCorrectAnswerInput(alphanumericAnswer[len - numberOfAlphanumericCharacters], decodedMorseCode, morseCodeAnswer[len - numberOfAlphanumericCharacters], userInput[0], lives);
                }
                numberOfAlphanumericCharacters--;
            }
            // The inputing session has finished, now check whether is this round won
            if(isThisRoundWon != 0){
                currentLevelWinningStreak++;
                totalWin++; // increment the total number of round won
                currentLevelTotalNumberOfRoundsWon++;
                // printf("currentLevelTotalNumberOfRoundsWon: %d\n", currentLevelTotalNumberOfRoundsWon);
                if( lives < 3 )
                {
                    lives++;
                }
            }
            set_colour(lives);
            printf("---------- Current Winning Streaks: %d -----------------------\n", currentLevelWinningStreak);
            // check if we should progress to the next level
            if(currentLevelWinningStreak >= 5){
                // update statistic
                if( currentLevelWinningStreak > maxWinningStreaks){
                    maxWinningStreaks = currentLevelWinningStreak;
                }
                if(currentLevelWinningStreak > currentLevelHighestWinStreak) {
                    currentLevelHighestWinStreak = currentLevelWinningStreak;
                }
                currentLevelWinningStreak = 0;
                if(level >= 4){
                    gameOn = 0; // finish the game
                    winScreen(maxWinningStreaks, totalWin, totalQuestions); // game finishes and print the success page
                    watchdog_update();
                    sleep_ms(3000);
                }else{

                    level++; // progress to next level
                    watchdog_update();
                    printStatisticBeforeNextLevel(currentLevelHighestWinStreak, currentLevelTotalNumberOfRoundsWon, currentLevelTotalNumberOfRoundsPlayed);
                    currentLevelHighestWinStreak = 0;
                    currentLevelTotalNumberOfRoundsWon = 0;
                    currentLevelTotalNumberOfRoundsPlayed = 0;
                    nextLevel(lives, level);
                }
            }
    }
    }
    return 0;
}
